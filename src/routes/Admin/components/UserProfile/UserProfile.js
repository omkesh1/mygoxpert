import React from "react";
import FilterTable from 'components/Table/FilterTable';

import MaterialTableDemo from 'components/Table/CoursesTable';


// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";
// core components
import GridItem from "components/Grid/GridItem.js";
import Card from "components/Card/Card.js";
import CardHeader from "components/Card/CardHeader.js";
import CardBody from "components/Card/CardBody.js";
import { card } from "assets/jss/material-dashboard-react";


export default function UserProfile() {
  
  const styles = {
    cardCategoryWhite: {
      color: "rgba(255,255,255,.62)",
      margin: "0",
      fontSize: "14px",
      marginTop: "0",
      marginBottom: "0"
    },
    cardTitleWhite: {
      color: "#FFFFFF",
      marginTop: "0px",
      minHeight: "auto",
      fontWeight: "300",
      fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
      marginBottom: "3px",
      textDecoration: "none"
    }
  };
  
  const useStyles = makeStyles(styles);
  const classes = useStyles();
  
  return (
    <div>
      <GridItem xs={12} sm={12} md={12}>
          <Card>
            <CardHeader color="primary">
                <h4 className={classes.cardTitleWhite}>User Table 1</h4>
                <p className={classes.cardCategoryWhite}>Details</p>
            </CardHeader>
            
            <GridItem xs={12} sm={10} md={10}>
              <CardBody>
                <FilterTable 
                    tableHeaderColor="primary"
                    //name,founded,rating,min_project_size,employees,hourly_rate
                    tableHead={["No","Name", "Email", "Designation", "User State", "Manage"]}
                    tableData={[["Sumit","Sumit@gmail.com","Dev","Active",""],
                                ["Amit","Amit@gmail.com","Dev","Inactive",""],
                                ["Ammey","Ammey@gmail.com","Dev","Disabled",""],
                                ["Anil","Anil@gmail.com","Dev","Active",""]
                              ]}
                />
              </CardBody>
            </GridItem>
          </Card>

          <Card>
              <CardHeader color="primary">
                <h4 className={classes.cardTitleWhite}>User Table 2</h4>
                <p className={classes.cardCategoryWhite}>Details</p>
              </CardHeader>                
              <CardBody>
                  <MaterialTableDemo tableHead={["No","Name", "Email", "Designation", "User State", "Manage"]} 
                                                
                                    tableData={[["Sumit","Sumit@gmail.com","Dev","Active",""],
                                                ["Amit","Amit@gmail.com","Dev","Inactive",""],
                                                ["Ammey","Ammey@gmail.com","Dev","Disabled",""],
                                                ["Anil","Anil@gmail.com","Dev","Active",""]
                                              ]}
                                    />
              </CardBody>
          </Card>
          
        </GridItem>
      
    </div>
  );
}

import React from "react";
import GridContainer from "components/Grid/GridContainer.js";

// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";
// core components
import GridItem from "components/Grid/GridItem.js";
import Card from "components/Card/Card.js";
import CardHeader from "components/Card/CardHeader.js";
import CardBody from "components/Card/CardBody.js";


export default function Invites() {

    const styles = {
        cardCategoryWhite: {
          color: "rgba(255,255,255,.62)",
          margin: "0",
          fontSize: "14px",
          marginTop: "0",
          marginBottom: "0"
        },
        cardTitleWhite: {
          color: "#FFFFFF",
          marginTop: "0px",
          minHeight: "auto",
          fontWeight: "300",
          fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
          marginBottom: "3px",
          textDecoration: "none"
        }
      };
      const useStyles = makeStyles(styles);
      const classes = useStyles();
    
  return (
    <GridContainer>
      <GridItem xs={12} sm={12} md={12}>
      <Card>
            <CardHeader color="primary">
                <h4 className={classes.cardTitleWhite}>Invites</h4>
                <p className={classes.cardCategoryWhite}>Invites Details</p>
            </CardHeader>
            
            <GridItem xs={12} sm={10} md={10}>
              <CardBody>
                This is Invites Page
              </CardBody>
            </GridItem>
          </Card>
      </GridItem>
    </GridContainer>
  );
}
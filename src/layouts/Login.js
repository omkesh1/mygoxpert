import React,{useState} from 'react';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
//import Link from '@material-ui/core/Link';
import Grid from '@material-ui/core/Grid';
import Box from '@material-ui/core/Box';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import { Redirect } from "react-router-dom";

const superagent = require("superagent");

// function Copyright() {
//   return (
//     <Typography variant="body2" color="textSecondary" align="center">
//       {'Copyright © '}
//       <Link color="inherit" href="#">
//         GoXpert
//       </Link>{' '}
//       {new Date().getFullYear()}
//       {'.'}
//     </Typography>
//   );
// }

const useStyles = makeStyles(theme => ({
  '@global': {
    body: {
      backgroundColor: theme.palette.common.white,
    },
  },
  paper: {
    marginTop: theme.spacing(8),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  }
}));



export default function SignIn() {
    const classes = useStyles();
    const [email, setEmail] = useState({"email":"","password":""});
    const [password, setPassword] = useState();
    
    const getRoute = () => {
      //return window.location.pathname === "/login";
      
    };

    const handleInputChange = (event) => {
        if(event.target.name === "email"){
            setEmail(event.target.value);
        }else if(event.target.name === "password"){
            setPassword(event.target.value);
        }
    }

    function login(event){
        //event.preventDefault();
        const formData = {"Email":email,"Password":password};
        //console.log("formData->>",formData);
        superagent
          .post('http://localhost/data.php')
          .set('Content-Type', 'application/x-www-form-urlencoded')
          .send(formData)
          .end(function(err, res){
            const result = JSON.parse(res.text);
            if(result.userType === "Admin" && result.token.length !== 0){
                localStorage.setItem('JwtToken',result.token);
                localStorage.setItem('userType',result.userType);
            }else if(result.userType === "User" && result.token.length !== 0){
              localStorage.setItem('JwtToken',result.token);
              localStorage.setItem('userType',result.userType);
            }else{
                localStorage.clear();
            }
          });
    }
  
    if(localStorage.userType === 'Admin'){
      return <Redirect to="/admin" />;
    }
    else if(localStorage.userType === 'User'){
      return <Redirect to="/user" />;
    } 


  return (
    <Container component="main" maxWidth="xs">
      {getRoute()}
      <CssBaseline />
      <div className={classes.paper}>
        <Avatar className={classes.avatar}>
          <LockOutlinedIcon />
        </Avatar>
        <Typography component="h1" variant="h5">
          Sign in
        </Typography>
        <form className={classes.form} Validate onSubmit={login} >
          
          <TextField
            onChange={handleInputChange}
            variant="outlined"
            margin="normal"
            required
            fullWidth
            id="email"
            label="Email Address"
            name="email"
            autoComplete="email"
            autoFocus
          />
          <TextField
            onChange={handleInputChange}
            variant="outlined"
            margin="normal"
            required
            fullWidth
            name="password"
            label="Password"
            type="password"
            id="password"
            autoComplete="current-password"
          />
          <FormControlLabel
            control={<Checkbox value="remember" color="primary" />}
            label="Remember me"
          />
          <Button
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            className={classes.submit}
          >
            Sign In
          </Button>
          <Grid container>
            <Grid item xs>
              {/* <Link href="#" variant="body2">
                Forgot password?
              </Link> */}
            </Grid>
            <Grid item>
              {/* <Link href="#" variant="body2">
                {"Don't have an account? Sign Up"}
              </Link> */}
            </Grid>
          </Grid>
        </form>
      </div>
      <Box mt={8}>
        {/* <Copyright /> */}
      </Box>
    </Container>
  );
}
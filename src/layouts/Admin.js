import React from "react";
import { Switch, Route, Redirect } from "react-router-dom";
// creates a beautiful scrollbar
import PerfectScrollbar from "perfect-scrollbar";
import "perfect-scrollbar/css/perfect-scrollbar.css";
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";
// core components
import Navbar from "components/Navbars/Navbar.js";
import Sidebar from "components/Sidebar/Sidebar.js";
import FixedPlugin from "components/FixedPlugin/FixedPlugin.js";
//import routes from "routes.js";
import styles from "assets/jss/material-dashboard-react/layouts/adminStyle.js";
import bgImage from "assets/img/sidebar-2.jpg";
import logo from "assets/img/Go.png";

//adding redux by omkesh
import { connect } from  'react-redux'
import { anotherName } from '../store/action/myActions.js';

// ~~Admin routes components~~
// @material-ui/icons
import Dashboard from "@material-ui/icons/Dashboard";
import Person from "@material-ui/icons/Person";

// core components/views for Admin layout
import DashboardPage from "../routes/Admin/components/Dashboard/Dashboard.js";
import UserProfile from "../routes/Admin/components/UserProfile/UserProfile.js";
import Invites from "../routes/Admin/components/Invites/Invites.js";
import Courses from "../routes/Admin/components/Courses/Courses.js";
import { Divider } from "@material-ui/core";

const AdminRoutes = [
  {
    path: "/dashboard",
    name: "Dashboard",
    icon: Dashboard,
    component: DashboardPage,
    layout: "/admin"
  },
  {
    path: "/users",
    name: "Users",
    icon: Person,
    component: UserProfile,
    layout: "/admin"
  },
  {
    path: "/invites",
    name: "Invites",
    icon: "mobile_screen_share",
    component: Invites,
    layout: "/admin"
  },
  {
    path: "/courses",
    name: "Courses",
    icon: "assignment",
    component: Courses,
    layout: "/admin"
  }
];


let ps;

const switchRoutes = (
  <Switch>
    {AdminRoutes.map((prop, key) => {
      if (prop.layout === "/admin") {
        return (<Route
                  path={prop.layout + prop.path}
                  component={prop.component}
                  key={key}
                />);
      }
      return null;
    })}
    {/* <Redirect from="/admin" to="/admin/dashboard" /> */}
  </Switch>
);

const useStyles = makeStyles(styles);


function Admin({...rest}) {
  // styles
  const classes = useStyles();
  // ref to help us initialize PerfectScrollbar on windows devices
  const mainPanel = React.createRef();
  // states and functions
  const [image, setImage] = React.useState(bgImage);
  const [color, setColor] = React.useState("blue");
  const [fixedClasses, setFixedClasses] = React.useState("dropdown");
  const [mobileOpen, setMobileOpen] = React.useState(false);
  const handleImageClick = image => {
    setImage(image);
  };
  const handleColorClick = color => {
    setColor(color);
  };
  const handleFixedClick = () => {
    if (fixedClasses === "dropdown") {
      setFixedClasses("dropdown show");
    } else {
      setFixedClasses("dropdown");
    }
  };
  const handleDrawerToggle = () => {
    setMobileOpen(!mobileOpen);
  };
  const getRoute = () => {
    return window.location.pathname !== "/admin/maps";
  };
  const resizeFunction = () => {
    if (window.innerWidth >= 960) {
      setMobileOpen(false);
    }
  };
  // initialize and destroy the PerfectScrollbar plugin
  React.useEffect(() => {
    if (navigator.platform.indexOf("Win") > -1) {
      ps = new PerfectScrollbar(mainPanel.current, {
        suppressScrollX: true,
        suppressScrollY: false
      });
      document.body.style.overflow = "hidden";
    }
    window.addEventListener("resize", resizeFunction);
    // Specify how to clean up after this effect:
    return function cleanup() {
      if (navigator.platform.indexOf("Win") > -1) {
        ps.destroy();
      }
      window.removeEventListener("resize", resizeFunction);
    };
  }, [mainPanel]);

  if(localStorage.userType !== 'Admin'){
    return <Redirect to="/login" />;
  }

  return (
    <div className={classes.wrapper}>
      {/* **** 1.Main Admin Sidebar *******/}
      <Sidebar
        routes={AdminRoutes}
        logoText={"GoExpert"}
        logo={logo}
        image={image}
        handleDrawerToggle={handleDrawerToggle}
        open={mobileOpen}
        color={color}
        {...rest}
      />

      {/* **** 2.Container *******/}
      <div className={classes.mainPanel} ref={mainPanel}>
        {/* **** 2.0.Navbar *******/}
        <Navbar
          routes={AdminRoutes}
          handleDrawerToggle={handleDrawerToggle}
          bgColor={color}
          {...rest}
        />
        
        {/* **** 2.1. Page Contain *******/}
        {/* On the /maps route we want the map to be on full screen - this is not possible if the content and conatiner classes are present because they have some paddings which would make the map smaller */}
        {getRoute() ? (
          <div className={classes.content}>
            <div className={classes.container}>{switchRoutes}</div>
          </div>
        ) : (
          <div className={classes.map}>{switchRoutes}</div>
        )}
        
        {/* **** 2.1. Theme changer  *******/}
        <FixedPlugin
          handleImageClick={handleImageClick}
          handleColorClick={handleColorClick}
          bgColor={color}
          bgImage={image}
          handleFixedClick={handleFixedClick}
          fixedClasses={fixedClasses}
        />
        
        <button onClick={()=>{rest.changeName()}}> change reducer value</button>
        <div><h2>State-->>{rest.name}</h2></div>

      </div>
    </div>
  );
}

const mapDispachToProp = (dispatch) =>{
  return {
    changeName:() => {dispatch(anotherName())}
  }
}

const mapStateToProps = (state) =>{
  return {
    name:state.name,
  }
}

export default connect(mapStateToProps,mapDispachToProp) (Admin);

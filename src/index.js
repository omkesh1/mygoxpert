import React from "react";
import ReactDOM from "react-dom";
import { createBrowserHistory } from "history";
import { Router, Route, Switch, Redirect } from "react-router-dom";
import './loader.js'

//addding redux by omkesh 
import { createStore,applyMiddleware,compose,combineReducers } from 'redux';
import { Provider } from 'react-redux';
import nameReducer from './store/reducer/nameReducer';
import thunk from 'redux-thunk';

// core components
import Admin from "layouts/Admin.js";
import User from "layouts/User";
import SignIn from "layouts/Login.js";

import "assets/css/material-dashboard-react.css?v=1.8.0";

const hist = createBrowserHistory();

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const store = createStore(nameReducer,composeEnhancers(applyMiddleware(thunk)));

console.log("Localstorage-->",localStorage.getItem("userType"));

// const loadPage = () =>{
//   if(localStorage.getItem("userType") == "Admin"){
//     return Admin;
//   }else if(localStorage.getItem("userType") == "User"){
//     return User;
//   }
//   return SignIn;
// }


ReactDOM.render(
  <Provider store={store}>
    <Router history={hist}>
      <Switch>
        <Route path="http://localhost:8000/login" component={SignIn} />
        <Route path="http://localhost:8000/user" component={User} />
        <Route path="http://localhost:8000/admin" component={Admin} />
        <Redirect from="/" to="/login" />
        {/* <Redirect from="/" to="/admin/dashboard" /> */}
        {/* <Route path="/user" component={User} /> */}
      </Switch>
    </Router>
  </Provider>,
  document.getElementById("root")
);

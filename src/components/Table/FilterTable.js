import React,{useState} from "react";
import PropTypes from "prop-types";
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import Search from "@material-ui/icons/Search";
// core components
import styles from "assets/jss/material-dashboard-react/components/tableStyle.js";
import Button from "components/CustomButtons/Button.js";
import { TablePagination } from '@material-ui/core';


const useStyles = makeStyles(styles);



export default function FilterTable(props) {
  const classes = useStyles();
  const { tableHead, tableData, tableHeaderColor } = props;
  //console.log(tableData);

  const [name,filterName] = useState();
  const [email,filterEmail] = useState();
  const [designation,filterDesign] = useState();
  const [userState,filterState] = useState();


  const [filterTableData,SetTableData] = useState(tableData);
  
  

  function serchByName(e){
    //filterName(e.target.value);

    //console.log(name);
    
    if(e.target.value.length == 0){
      return null;
    }else{
      filterTableData.map(function(value,key){
        if(value[1].toLowerCase().startsWith(e.target.value)){
          return console.log(value[1]);
        }else{
          return null;
        }
      });
    }
  }

  return (
    <div className={classes.tableResponsive}>
      
      <Table className={classes.table}>
        {tableHead !== undefined ? (
          <TableHead className={classes[tableHeaderColor + "TableHeader"]}>
              <TableRow className={classes.tableHeadRow}>
                {tableHead.map((prop, key) => {
                  return (
                    <TableCell
                      className={classes.tableCell + " " + classes.tableHeadCell}
                      key={key}
                    >
                      {prop}  
                    </TableCell>
                  );
                })}
              </TableRow >
              
                  <TableRow className={classes.tableHeadRow}>
                    <TableCell className={classes.tableCell + " " + classes.tableHeadCell}>
                    </TableCell>
                    
                    <TableCell className={classes.tableCell + " " + classes.tableHeadCell} >
                      <input placeholder="Serach Name" onChange={serchByName} style={{ width: '50%' }} />
                    </TableCell>

                    <TableCell className={classes.tableCell + " " + classes.tableHeadCell} >
                      <input placeholder="Serach Email" style={{ width: '60%' }} />
                    </TableCell>
                    
                    <TableCell className={classes.tableCell + " " + classes.tableHeadCell} >
                      <input placeholder="Serach Designation" style={{ width: '70%' }} />
                    </TableCell>
                    
                    <TableCell className={classes.tableCell + " " + classes.tableHeadCell} >
                      <div>
                      <select>
                        <option>Select State</option>
                        <option value="Created">Created</option>
                        <option value="Invited">Invited</option>
                        <option value="Active">Active</option>
                        <option value="Disabled">Disabled</option>
                        <option value="Deleted">Deleted</option>
                      </select>
                      </div>
                    </TableCell>
                    
                    <TableCell className={classes.tableCell + " " + classes.tableHeadCell} >
                      <Button color="withe" aria-label="edit" justIcon round>
                        <Search />
                      </Button>
                    </TableCell>
                  </TableRow>
            </TableHead>
        ) : null}

    <TableBody>
          {filterTableData.map((prop, key) => {
            prop.unshift(key+1);
            return (
              <TableRow key={key} className={classes.tableBodyRow}>
                {prop.map((prop, key) => {
                  return (
                    <TableCell className={classes.tableCell} key={key}>
                      {prop}
                    </TableCell>
                  );
                })}
              </TableRow>
            );
          })}
        </TableBody>
        <TablePagination rowsPerPageOptions={[10, 50,{ value: -1, label: 'All' }]}/>
      </Table>
    </div>
  );
}

FilterTable.defaultProps = {
  tableHeaderColor: "gray"
};

FilterTable.propTypes = {
  tableHeaderColor: PropTypes.oneOf([
    "warning",
    "primary",
    "danger",
    "success",
    "info",
    "rose",
    "gray"
  ]),
  tableHead: PropTypes.arrayOf(PropTypes.string),
  tableData: PropTypes.arrayOf(PropTypes.arrayOf(PropTypes.string))
};

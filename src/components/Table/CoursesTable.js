import React from 'react';
import MaterialTable from 'material-table';

export default function MaterialTableDemo(props) {
  
  const { tableHead, tableData, tableHeaderColor } = props;
  // console.log("tableHead-->",tableHead);
  // console.log("tableData-->",tableData);
  // tableHead.map(function(value,key){
  //   return console.log(value,key)
  // });
  // let col = [];
  // tableHead.map(function(value,key){
  //   col.push({"title":value,"field":value,"tableData":""});
  // });
  
  // console.log(col);
  
  // const [state, setState] = React.useState({
  //   columns: [col],
  //   data: [],
  // });
  // console.log(state.columns);
  
  const [state, setState] = React.useState({
    columns: [
      { title: 'Name', field: 'name' },
      { title: 'Email', field: 'email' },
      { title: 'Designation', field: 'designation' },
      { title: 'State', field: 'state' }
      // { title: 'Birth Year', field: 'birthYear', type: 'numeric' },
      // { title: 'Birth Place',field: 'birthCity', lookup: { 34: 'Pune', 63: 'Pune' }},
    ],
    data: [
      { name: 'Omkesh', email: 'Omkesh@gmail.com',designation:"Dev",state:"Active" },
      { name: 'Amit', email: 'Amit@gmail.com',designation:"Dev", state:"Inactive"},
    ],
  });


  /*editable={{
        onRowAdd: newData =>
          new Promise(resolve => {
            setTimeout(() => {
              resolve();
              setState(prevState => {
                const data = [...prevState.data];
                data.push(newData);
                return { ...prevState, data };
              });
            }, 600);
          }),
        onRowUpdate: (newData, oldData) =>
          new Promise(resolve => {
            setTimeout(() => {
              resolve();
              if (oldData) {
                setState(prevState => {
                  const data = [...prevState.data];
                  data[data.indexOf(oldData)] = newData;
                  return { ...prevState, data };
                });
              }
            }, 600);
          }),
        onRowDelete: oldData =>
          new Promise(resolve => {
            setTimeout(() => {
              resolve();
              setState(prevState => {
                const data = [...prevState.data];
                data.splice(data.indexOf(oldData), 1);
                return { ...prevState, data };
              });
            }, 600);
          }),
      }} */

  state.columns.map(function(value,key){
    
    //console.log("state value-->",value);
  });

  //console.log("Columns>>",state.columns);
  // console.log("data>>",state.data);
  

  return (
    <MaterialTable
      title="User Table 2"
      columns={state.columns}
      data={state.data}
      editable={{
        onRowAdd: newData =>
          new Promise(resolve => {
            setTimeout(() => {
              resolve();
              setState(prevState => {
                const data = [...prevState.data];
                data.push(newData);
                return { ...prevState, data };
              });
            }, 600);
          }),
        onRowUpdate: (newData, oldData) =>
          new Promise(resolve => {
            setTimeout(() => {
              resolve();
              if (oldData) {
                setState(prevState => {
                  const data = [...prevState.data];
                  data[data.indexOf(oldData)] = newData;
                  return { ...prevState, data };
                });
              }
            }, 600);
          }),
        onRowDelete: oldData =>
          new Promise(resolve => {
            setTimeout(() => {
              resolve();
              setState(prevState => {
                const data = [...prevState.data];
                data.splice(data.indexOf(oldData), 1);
                return { ...prevState, data };
              });
            }, 600);
          }),
      }}
    />
  );
}
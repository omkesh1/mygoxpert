import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Breadcrumbs from '@material-ui/core/Breadcrumbs';
import Link from '@material-ui/core/Link';
import HomeIcon from '@material-ui/icons/Home';
import Icon from "@material-ui/core/Icon";
import Person from "@material-ui/icons/Person";


function handleClick(event) {
  //event.preventDefault();
  console.info('You clicked a breadcrumb.');
}


export default function IconBreadcrumbs(props) {
    const { location, adminRoutes } = props;
    var path = location.pathname.split("/");
    const useStyles = makeStyles(theme => ({
    root: {
        marginLeft:theme.spacing(1),
        padding: theme.spacing(1,1),
        width: 1050,
        display: 'flex',
        backgroundColor:props.bgColor
    },
    link: {
        color:"white",
        display: 'flex',
    },
    icon: {
        color:"white",
        marginRight: theme.spacing(0.5),
        width: 20,
        height: 20,
    },
    }));

  const classes = useStyles();

  var icon = [];
  adminRoutes.map(function(val,key){
    return icon[val.name.toLowerCase()] = val.icon;
  });
  
  function getIcon(value){
    return typeof icon[value] === "string" ? <Icon className={classes.icon}>{icon[value]}</Icon> : <Person className={classes.icon}/>;
  }
  
  


  return (
    <Paper elevation={0} className={classes.root }>
      <Breadcrumbs aria-label="breadcrumb">
        {path.map(function(value,key){
            if(value != 0){
              if(value === "admin"){
                  value = "Dashboard";
                  return (<Link color="inherit" href={"/admin/"+value.toLowerCase()} onClick={handleClick} className={classes.link}>
                              <HomeIcon className={classes.icon} />{value.toUpperCase()}
                          </Link>);
              }else if(value === "dashboard"){
                return null;
              }
              return (<Link color="inherit" href={"/admin/"+value.toLowerCase()} onClick={handleClick} className={classes.link}>
                          {getIcon(value)}{value.toUpperCase()}
                      </Link>);
          }
        })}
      </Breadcrumbs>
    </Paper>
  );
}